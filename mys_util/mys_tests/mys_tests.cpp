//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software
//
//  This is free software; you can redistribute it and/or modify it under
//  the terms of the GNU General Public License  as published by the Free
//  Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.
//==============================================================================

#include <iostream>
#include <ranges>
#include <sstream>
#include <fstream>
#include <functional>
#include <utility>

#include "fp_utility.h"
#include "Line.h"

#include "test_data.h"

//---------------------------------------------------------------------------------------------------------------------------
// use input from a file or a char array
#define use_file 1

// directly execute the view or at end of pipeline
#define exec_view_direct 0

void read_to_output() {

#if use_file
   std::ifstream test_file("../test_data.txt");
#else
   std::istringstream test_file(test_data);
#endif

#if exec_view_direct
   vws::istream<mys::Line>(test_file) | mys::emit<mys::nl> | mys::EvalView();
#else
   auto emit_test =
      std::ranges::views::istream<mys::Line>(test_file) | mys::emit_nl;
   mys::eval_view(emit_test);
#endif
   std::cout << mys::nl << mys::nl;
}

void filter_command() {
   std::istringstream test_file(test_data);

   const char test {'$'};
   auto char_filter = [test](auto&& value) {
      return value.mLine[0] == test;
   };

   vws::istream<mys::Line>(test_file) | vws::filter(char_filter) |
   mys::emit_nl | mys::eval();
   std::cout << mys::nl << mys::nl;
}

//===========================================================================================================================
template<typename Function, typename ...CapturedArgs>
class curried {
public:
   explicit curried(Function function, CapturedArgs... args) : m_function(
      function), m_captured(std::make_tuple(args...)) {
   }

   curried(Function function, std::tuple<CapturedArgs...> args) : m_function(
      function), m_captured(std::move(args)) {
   }

   template<typename ...NewArgs>
   auto operator()(NewArgs&& ...args) const {
      auto new_args = std::make_tuple(std::forward<NewArgs>(args)...);
      auto all_args = std::tuple_cat(m_captured, std::move(new_args));

      if constexpr (std::is_invocable_v<Function, CapturedArgs...,
         NewArgs...>) {
         return std::apply(m_function, all_args);

      } else {
         return curried<Function, CapturedArgs..., NewArgs...>(m_function,
                                                               all_args);
      }
   }

private:
   Function m_function;
   std::tuple<CapturedArgs...> m_captured;
};

class callable_test {
public:
   template<typename T1, typename T2, typename T3>
   auto operator()(T1 x, T2 y, T3 z) const {
      return x + y + z;
   }

   template<typename T1, typename T2>
   auto operator()(T1 x, T2 y) const {
      return x + y;
   }
};

void curry_template() {
   auto less_curried = curried(std::less<>());

   std::cout << less_curried(42, 1) << mys::nl;

   auto greater_than_42 = less_curried(42);

   std::cout << greater_than_42(1.0) << mys::nl;
   std::cout << greater_than_42(100.0) << mys::nl;

   callable_test ct;

   auto ct_curried = curried(ct);

   std::cout << ct_curried(1)(2, 3) << mys::nl;

   auto ct_curried_one = curried(ct, 1);

   std::cout << ct_curried_one(2, 3) << mys::nl;
}

//---------------------------------------------------------------------------------------------------------------------------
int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) {
   std::cout << std::boolalpha;

//    read_to_output();
//    filter_command();
   curry_template();

   return 0;
}
