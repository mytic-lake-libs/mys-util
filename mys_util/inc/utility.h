//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software
//
//  This is free software; you can redistribute it and/or modify it under
//  the terms of the GNU General Public License  as published by the Free
//  Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.
//==============================================================================
#pragma once

#include <iostream>
#include "inc/Trace.h"

namespace mys {
  //---------------------------------------------------------------------------------------------------------------------------
#ifndef mys_chars
#define mys_chars

  constexpr auto tab {'\t'};
  constexpr auto nl {'\n'};
  constexpr auto sp {' '};
  constexpr auto comma {", "};
#endif

  //---------------------------------------------------------------------------------------------------------------------------
  [[maybe_unused]] auto
  check_expected(auto const& expected, auto const& received,
                 std::string const& msg = "") -> bool {
     bool is_as_expected = (expected == received);
     std::cout << msg << sp << (is_as_expected ? "Ok!  " : "Bad! ") << nl;

     if (!is_as_expected) {
        std::cout << "expected: [" << expected << ']' << nl  //
                  << "received [" << received << ']' << nl << nl; //
     }

     return is_as_expected;
  }

  //------------------------------------------------
  [[maybe_unused]]
  [[nodiscard("A bool variable is needed to hold the status of announce")]]
  inline
  auto say_it(bool& announce, char const* text) -> bool {
     if (announce) {
        tdbg << code_line << text << nl;
     }
     return announce = false;
  }

}   // end mys
