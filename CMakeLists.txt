cmake_minimum_required(VERSION 3.27)
project(mys_utils)

add_compile_options(-Wextra -pedantic -Werror -fmodules-ts -std=c++23)

#add_subdirectory(mys_util/mys_tests)
#add_subdirectory(Trace/trace_tests)
