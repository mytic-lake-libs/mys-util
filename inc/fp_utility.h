//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software                                   =
//                                                                             =
//  This is free software; you can redistribute it and/or modify it under      =
//  the terms of the GNU General Public License  as published by the Free      =
//  Software Foundation; either version 3 of the License, or (at your          =
//  option) any later version.                                                 =
//                                                                             =
//  This is distributed in the hope that it will be useful, but WITHOUT        =
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or      =
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License      =
//  for more details.                                                          =
//                                                                             =
//  You should have received a copy of the GNU General Public License          =
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.        =
//==============================================================================
#pragma once

//
// Created by rmerriam on 1/10/23.
//
#include <iostream>
#include <ranges>
#include <iomanip>

#include "utility.h"
//---------------------------------------------------------------------------------------------------------------------------
namespace rng = std::ranges;
namespace vws = std::views;

// NOLINTNEXTLINE(cppcoreguidelines-interfaces-global-init)

namespace mys {
   //---------------------------------------------------------------------------------------------------------------------------
   struct [[maybe_unused]] eval {
      friend auto operator|(rng::range auto&& view, eval) noexcept {
         for ([[maybe_unused]] auto v: view) {}
      }
   };
   //---------------------------------------------------------------------------------------------------------------------------
   [[maybe_unused]] void eval_view(rng::viewable_range auto&& view) noexcept {
      view | eval();
   }
   //---------------------------------------------------------------------------------------------------------------------------
   constexpr auto emit = [ ](char const delim = sp) {
      return vws::filter(    //
         [delim](auto&& value) noexcept {
            std::cout << value << delim;
            return true;
         });
   };
   //---------------------------------------------------------------------------------------------------------------------------
   constexpr auto emit_char = [ ]() {
      return vws::filter(    //
         [ ](auto&& value) noexcept {
            std::cout << value;
            return true;
         });
   };
   //---------------------------------------------------------------------------------------------------------------------------
   constexpr auto quote = [ ]() {
      return vws::filter(    //
         [ ](auto&& value) noexcept {
            std::cout << '[' << value << "] ";
            return true;
         });
   };
   //---------------------------------------------------------------------------------------------------------------------------
   auto emit_nl = emit(nl);
   auto emit_tab = emit(tab);
}   // end mys
