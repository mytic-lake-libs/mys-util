//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software                                   =
//                                                                             =
//  This is free software; you can redistribute it and/or modify it under      =
//  the terms of the GNU General Public License  as published by the Free      =
//  Software Foundation; either version 3 of the License, or (at your          =
//  option) any later version.                                                 =
//                                                                             =
//  This is distributed in the hope that it will be useful, but WITHOUT        =
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or      =
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License      =
//  for more details.                                                          =
//                                                                             =
//  You should have received a copy of the GNU General Public License          =
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.        =
//==============================================================================
#pragma once

//
// Created by rmerriam on 1/10/23.
//

namespace mys {
   //---------------------------------------------------------------------------------------------------------------------------
#ifndef mys_chars
#define mys_chars

   constexpr auto tab {'\t'};
   constexpr auto nl {'\n'};
   constexpr auto sp {' '};
   constexpr auto comma {", "};
#endif
   //---------------------------------------------------------------------------------------------------------------------------
   [[maybe_unused]] bool
   check_expected(auto const& expected, auto const& received, std::string const& msg = "") {
      bool is_as_expected = (expected == received);
      std::cout << (is_as_expected ? "Ok!  " : "Bad! ")    //
                << "expected: [" << expected << ']' << sp  //
                << "received [" << received << ']' << sp  //
                << msg << nl;    //;
      return is_as_expected;
   }

}   // end mys
