//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software                                   =
//                                                                             =
//  This is free software; you can redistribute it and/or modify it under      =
//  the terms of the GNU General Public License  as published by the Free      =
//  Software Foundation; either version 3 of the License, or (at your          =
//  option) any later version.                                                 =
//                                                                             =
//  This is distributed in the hope that it will be useful, but WITHOUT        =
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or      =
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License      =
//  for more details.                                                          =
//                                                                             =
//  You should have received a copy of the GNU General Public License          =
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.        =
//==============================================================================

//
// Created by rmerriam on 1/10/23.
//

#pragma once

/*
 *  class to allow reading lines of data into container or view
 *
 *  input and output stream operators provided to read and print lines of data
 */
namespace mys {

   struct Line {
      std::string mLine;

      operator std::string() const {
         return mLine;
      }
      friend std::ostream& operator<<(std::ostream& os, Line const& l) {
         os << l.mLine;
         return os;
      }
      friend std::istream& operator>>(std::istream& is, Line& l) {
         std::getline(is, l.mLine);
         return is;
      }
   };
}
